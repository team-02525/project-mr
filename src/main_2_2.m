
function main_2_2(test_images) 
% MAIN_2_2 A helper function which solves assignment 2.2 and generates
% a plot of each test image and its frequency domain
%
% Params:
%    'test_images': Cell array of images

% Number of test images
N = length(test_images);

% Plot rows of image and transformed image
for i = 1:N
    % Get image
    image = test_images{i};
    
    % Apply 2D DFT to image
    transformed_image = fft2(image);
    
    % Apply fft-shift and logarithmically scale image
    shifted_image = log(abs(fftshift(transformed_image)));
    
    % Plot original image
    subplot(N,2, 2*(i-1)+1)
    imshow(image);
    title("Image"); 
    
    % Plot shifted and transformed image
    subplot(N,2, 2*(i-1)+2)
    imagesc(shifted_image);
    title("Centered frequency domain"); 
        
end
end
