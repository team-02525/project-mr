function noisy_im = addnoise(im, tau)
% ADDNOISE simulates noise, be adding normally distributed noise
% (assuming real noise is additive) to n x n DFT singal
%
% Params:
% 'im': n x n DFT signal
% 'tau': percentage added noise. Has to be a float in the range [0,1]
%        and a string formatted as "N%" where N is a float in the range [0,100]
%
% Returns:
% 'noisy_im': DFT signal with added noise

% Check whether input is a string in the correct format and
% convert to a double in the range [0,1]
if isstring(tau) || ischar(tau)
    if tau(end) == '%'
        % Convert to double
        tau = str2double(tau(1:end-1))/100;
    else
       error('The given percentage is not of the right format'); 
    end 
end

% Check if percentage is scalar in the range [0,1]
% If str2double failed NaN is not scalar
if ~isscalar(tau) && (0 <= tau ) && (tau >= 1)
    error('Tau is an invalid type or out of range')
end
    
% Assert that the image is quadratic
[n, m] = size(im);

if n ~= m
   error('2D DFT signal is not quadratic');
end

% Generate normally distributed real noise
A = randn(n,n);

% Normalize the noise by the frobenius norm
A = A / norm(A, 'fro');

% Calculate normalised percentage of noise to be added
e_real = tau * A * norm(real(im), 'fro');  

% If array is complex, equivalently add complex noise 
if ~isreal(im)
    B = randn(n,n);
    B = B / norm(B, 'fro');
    e_imag = tau * B * norm(imag(im), 'fro');
else
    % If image is real, set complex noise to zero
    e_imag = 0;
end

% Add noise to image
noisy_im = im + e_real + e_imag*1i;
