function main_3_2(data_dir, output_dir)
% MAIN_3_2 Reconstructs the slices the mouse and head scans, and places
% them as pictures of appropriate formats in output_dir.
%
% Params:
%   'data_dir': The directory where the slices are found.
%   'output_dir': The directory where the results are placed.

if ~ischar(data_dir) && ~isstring(data_dir)
    error("Directory path not valid.");
elseif ~ischar(output_dir) && ~isstring(output_dir)
    error("Directory path not valid.");
end

% Load the head slices.
head_data = load(data_dir + "/headhead.mat");
% Extract the head slices from the struct.
head_data = head_data.head;
% Reconstruct the head slices and normalize.
head = normalize_image(recon_volume(head_data));

% Same procedure for the mouse heart as for the head.
mouse_data = load(data_dir + "/mouseheart.mat");
mouse_data = mouse_data.mouse;
mouse = normalize_image(recon_volume(mouse_data));


% Create a slice montage of the head.
head_montage = montage(head);
% Save the head montage.
imwrite(head_montage.CData, output_dir + "/head-montage.png");
% Same for the mouse heart.
mouse_montage = montage(mouse);
imwrite(mouse_montage.CData, output_dir + "/mouse-montage.png");