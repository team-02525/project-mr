function main_2_6_a(test_image, grid_size)
% MAIN_2_6_a generates a grid of plots with noise and sampling in the
% range [0,1] and [0.1, 1] respectivly.
%
% Params:
% 'test_image': Image
% 'grid_size': Integer of images per axis in the grid

% Create vectors of noise and frac percentages respectivly
noise = linspace(0, 1, grid_size);
frac = linspace(1,0.1, grid_size);

% Generate subplots for each combination of noise and frac
for i = 1:grid_size
    for j = 1:grid_size
        % Get noise and frac percentage
        noise_pct = noise(i);
        frac_pct = frac(j);
        
        % Generate test image
        image = generate_test_image(test_image, noise_pct, frac_pct);
        
        % Get index for subplot
        idx = grid_size * (i-1) + j;
        
        % Plot image
        subplot(grid_size,grid_size, idx);
        imagesc(abs(test_image)), colormap gray;
        
        % Plot title with Noise and frac values
        title("Noise: " + noise_pct + " frac: " + frac_pct);
    end
end

end

function test_im = generate_test_image(im, noise, frac)
% GENERATE_TEST_IMAGE, a helper function to add noise and sample MRI-signal
% respectively
% 
% Params:
% 'im': Image to be added noise and sampled in frequency spectrum
% 'noise': percentage of noise to be added
% 'frac': perecntage of image to be sampled
% 
% Returns:
% 'test_im': Reconstruction of frequency spectrum with noise and sampling

% Transform image
transformed_image = fft2(im);

% Add noise 
noisy_transform = addnoise(transformed_image, noise);

% Sample signal (signal_limited expects fftshifted signal)
sampled_transform = signal_limited(fftshift(noisy_transform), frac);

% fftshift back again
test_im = ifft2(fftshift(sampled_transform));

end
