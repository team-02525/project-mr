function main_2_5(test_images) 
% MAIN_2_5 A helper function which solves assignment 2.5 and generates
% a plot of each test image, its frequency domain with and without sampling
% and a sampled reconstruction.
%
% Params:
%   'test_images': Cell array of images

% Number of test images
N = length(test_images);


% Plot rows of image and transformed image
for i = 1:N
    % Get image
    image = test_images{i};
    % Plot image
    subplot(N, 4, 4*(i-1)+1)
    imshow(image);
    title("Original image");

    % Apply 2D DFT to image
    transformed_image = fft2(image);
    
    % Apply fft-shift and logarithmically scale frequency spectrum
    shifted_image = log(abs(fftshift(transformed_image)));
    
    % Plot shifted and transformed image
    subplot(N,4, 4*(i-1)+2)
    imagesc(shifted_image);
    title("Centered frequency domain"); 
    
    % Sampled 10% of 2D DFT signal
    transformed_sampled_image = signal_limited(...
        fftshift(transformed_image), 0.1);

    % Apply fft-shift and logarithmically scale frequency spectrum
    shifted_sampled_image = log(abs(transformed_sampled_image));
 
    % Plot sampled 10% of 2D DFT signal
    subplot(N,4, 4*(i-1)+3)
    imagesc(shifted_sampled_image);
    title("Centered frequency domain w. sampling");
    
    % Generate reconstruction with sampling
    sampled_reconstruction = ifft2(fftshift(transformed_sampled_image));
    
    % Plot reconstruction with sampling
    subplot(N,4, 4*(i-1)+4)
    imshow(abs(sampled_reconstruction));
    title("Reconstructed image w. sampling");
    
end

end
