function result_matrix = place_matrix_in_matrix(source_matrix, dest_matrix, location)
% PLACE_MATRIX_IN_MATRIX Place a matrix into a specific location in a
% larger matrix.
% 
% Adds the matrices together as though the source matrix were a zero matrix
% except in an area the size of source_matrix with an upper left corner in
% location.
%
% Params:
%   'source_matrix': The matrix to place into the larger matrix. Must be
%   smaller or of the same size as the destination matrix.
%   'dest_matrix': The matrix to place the smaller matrix in.
%   'location': Where in the destination matrix to place the source matrix.


% Assert that source matrix is within bounds of destination matrix
if any(size(source_matrix) > size(dest_matrix))
    error("Source matrix must be smaller than the destination matrix");
elseif any(location+size(source_matrix) > size(dest_matrix))
    error("Inserted matrix goes beyond the edges of the result matrix");
elseif any(location < 0)
    error("Location cannot be negative");
end

[source_height, source_width] = size(source_matrix);
x = location(1);
y = location(2);

% The resulting state of the area where the matrix is to be placed.
result_area = dest_matrix(x:x+source_height-1,y:y+source_width-1)+source_matrix;

result_matrix = dest_matrix;
% Replaces the destination area with result_area.
result_matrix(x:x+source_height-1,y:y+source_width-1) = result_area;