function graph = generate_graph(image, num_plots, x_axis_effect, effect2, x_label)
% GENERATE_GRAPH Generates a graph with multiple plots and error on the
% y-axis. The x-axis represents how much x_axis_effect is applied.
% Different plots have different amounts of effect2 applied.
%
% Params:
%   'image': The image to apply the effects to.
%   'num_plots': The number of plots.
%   'x_axis_effect': Applied after effect2. The size of this effect is
%   represented by the x-axis.
%   'effect2': This effect is applied before x_axis_effect. The amount
%   differs between plots.
%   'x_label': Label for the x-axis.
%
% Returns:
%   'graph': Returns the generated graph

number_of_images = 100;
% Which effect sizes to use for the two effects.
effect1_size_array = (0:number_of_images)/number_of_images;
effect2_size_array = (0:num_plots)/num_plots;

title("Error from noise and sampling");
xlabel(x_label);
ylabel("Error");
hold on
% Create each plot
for i = 1:num_plots
    % This function takes some time, so it's nice to get some info about
    % how far it's gotten.
    disp(i);
    % The power of the second effect for this plot.
    effect2_size = effect2_size_array(i);
    % Lambda that applies the second effect at the right power, and the
    % first effect at the specified power.
    effect = @(im, effect_size) x_axis_effect(effect2(im, effect2_size), effect_size);
    % Generate affected images and find there error measures.
    [error, ans] = generate_altered_images(number_of_images, effect, image);
    plot(effect1_size_array, error);
end
hold off;
% Put the current graph into the return variable.
[graph, ans] = frame2im(getframe(gcf));