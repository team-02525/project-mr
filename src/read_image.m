function image = read_image(file_name)
% READ_IMAGE Reads an image from a provided filename
% Params:
%   'fileName': String indicating which file to read.
% Returns:
%   'image': A MxNx3 matrix with values between 0 and 1 symbolizing the
%   file as RGB vectors in a MxN matrix.

if ~ischar(file_name) && ~isstring(file_name)
    error("Filename not vaild");
end
image = double(imread(file_name))/255;
