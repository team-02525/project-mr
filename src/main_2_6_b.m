function main_2_6_b(test_image, num_points)
% MAIN_2_6_B Generates error graphs for noise and sampling.
%
% Params:
%   'output_dir': The directory in which to place the graphs.

% Generate vector of noise percentages
tau = linspace(0,1, num_points+1);

% Apply noise to image and calculate error measure
noise_effect = @(im, tau) create_noisy_image(im, tau);
[noise_error, noisy_test_images] = generate_altered_images(...
    num_points, noise_effect, test_image);

% Apply noise to image and calculate error measure
sample_effect = @(im, frac) create_sampled_image(im, frac);
[sampling_error, sampled_test_images] = generate_altered_images(...
    num_points, sample_effect, test_image);


% Create plot
subplot(2,1,1);
plot(tau, noise_error);
title('Error from noise');
xlabel('Noise');
ylabel('Error');

% Create plot
subplot(2,1,2);
plot(tau, sampling_error);
title('Error from sampling');
xlabel('Sampling');
ylabel('Error');
