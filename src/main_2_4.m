function main_2_4(test_images)
% MAIN_2_4 A helper function which solves assignment 2.3 & 2.4
% and generates a plot of each test image and its frequency domain with
% and without noise and a reconstruction of the noisy signal.
%
% Params:
%   'test_images': Cell array of images

% Number of test images
N = length(test_images);

% Plot rows of image and transformed image
for i = 1:N
    % Get image
    image = test_images{i};
    
    % Plot image
    subplot(N, 4, 4*(i-1)+1)
    imshow(image);
    title("Original image");
    
    % Apply 2D DFT to image
    transformed_image = fft2(image);
    
    % Apply fft-shift and logarithmically scale frequency spectrum
    shifted_image = log(abs(fftshift(transformed_image)));

    % Plot shifted and transformed image
    subplot(N,4, 4*(i-1)+2)
    imagesc(shifted_image);
    title("Centered frequency domain"); 
    
    % Add 1% noise to 2D DFT signal
    transformed_noisy_image = addnoise(transformed_image, 0.1);
    
    % Apply fft-shift and logarithmically scale noisy frequency spectrum
    shifted_noisy_image = log(abs(fftshift(transformed_noisy_image)));
 
    % Plot with 1% addded noise
    subplot(N,4, 4*(i-1)+3)
    imagesc(shifted_noisy_image);
    title("Centered frequency domain w. noise");
    
    % Generate reconstruction with noise
    noisy_reconstruction = ifft2(transformed_noisy_image);
    
    % Plot reconstruction with noise
    subplot(N,4, 4*(i-1)+4)
    imshow(abs(noisy_reconstruction));
    title("Reconstructed image w. noise");
    
end
