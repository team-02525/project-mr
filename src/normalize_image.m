function result = normalize_image(image, value)
% NORMALIZE_IMAGE Normalized the image to a given value. The normalized image
% will have 0 as its lowest value and value as its highest.
%
% Params:
%   'image': A matrix representing the image to be normalized. Doesn't
%   actually have to be an image though.
%   'value': The value to normalize the image to. DEFAULT: 1
%
% Returns:
%   'result': The normalized image.

if nargin == 1
    value = 1;
end


if ~isnumeric(image)
    error("image must be a numeric array.");
elseif ~isscalar(value) || value < 0
    error("value must be a non-negative number");
end

% Move image so 0 is its lowest value.
result = image-min(image, [], 'all');

result_max = max(result, [], 'all');
% If result_max is zero, avoid a division by zero error and return since
% all values are 0.
if result_max == 0
    return;
end

% Normalize
result = (result/result_max) * value;