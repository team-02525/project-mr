function noisy_image = create_noisy_image(image, tau)
% CREATE_NOISY_IMAGE Adds noise to an image by fourier transforming it,
% applying noise, and transforming back again.
%
% Params:
%   'image': The image to apply noise to.
%   'tau': The amount of noise.
%
% Returns:
%   'noisy_image': The noisy image.

transformed_image = fft2(image);

noisy_transform = addnoise(transformed_image, tau);

noisy_image = ifft2(noisy_transform);