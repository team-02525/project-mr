function sampled_image = create_sampled_image(image, frac)
% CREATE_SAMPLED_IMAGE Adds a low pass filter by sampling it in frequency
% space.
%
% Params:
%   'image': The image to apply noise to.
%   'frac': The amount of frequencies to keep.
%
% Returns:
%   'sampled_image': The sampled image.

% signal_limited expects fftshifted image.
transformed_image = fftshift(fft2(image));

sampled_transform = signal_limited(transformed_image, frac);

% fftshift back again.
sampled_image = ifft2(fftshift(sampled_transform));