function err = error_measure(x, x_hat)
% ERROR_MEASURE calculates error of reconstruction by the formula
% err = ||x - x_hat||_fro /||x||_fro
% where x and x_hat are of equal dimmensions
% 
% Params:
% 'x': ground truth image
% 'x_hat': Reconstructed image
% 
% Returns:
% 'err': Error measure

% Assert that images are of equal dimmensions
if ~all(size(x) == size(x_hat)) 
    error('Images must be of equal dimmensions');
end

err = norm(x - x_hat, 'fro') / norm(x, 'fro');

end