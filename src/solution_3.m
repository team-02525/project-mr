%% Create directory to save images to
mkdir("../results-3/");

%% Load mouse and head data files
mouse_data = load("../data/mouseheart.mat");
mouse_data = mouse_data.mouse;
head_data = load("../data/headhead.mat");
head_data = head_data.head;

%% Reconstruct
% Heart and head data are fftshifted, so they need to be fftshifted back
% before reconstruction. recon_volume does this for us.
mouse = recon_volume(mouse_data);
head = recon_volume(head_data);

%% Normalize reconstructions
mouse = normalize_image(abs(mouse));
head = normalize_image(abs(head));

%% Save reconstructed mouse heart and human head.
mouse_montage = montage(mouse);
imwrite(mouse_montage.CData, "../results-3/mouse-heart-montage.png");
head_montage = montage(head);
imwrite(head_montage.CData, "../results-3/head-montage.png");

%% Get head slices
[xy, xz, yz] = ortho_slices(head, 1, 128, 128);

%% Save head slice image
subplot(1,3,1);
imshow(xy);
title('XY-plane, slice 1');

subplot(1,3,2);
imshow(xz);
title('XZ-plane, slice 128');

subplot(1,3,3);
imshow(yz);
title('YZ-plane, slice 128');
print("../results-3/head-slices", "-dpng");

%% Load unknown data files
A_data = load("../data/A.mat");
A_data = A_data.A;
B_data = load("../data/B.mat");
B_data = B_data.B;

%% Reconstruct
% A and B are not fftshifted, so recon_volume needs to know not to fftshift 
% them.
A = recon_volume(A_data, 'all', false);
B = recon_volume(B_data, 'all', false);

%% Normalize reconstructions
A = normalize_image(abs(A));
B = normalize_image(abs(B));

%% Get A slices
[xy, xz, yz] = ortho_slices(A, 128, 128, 128);

%% Save head slice image
subplot(1,3,1);
imshow(xy);
title('XY-plane, slice 128');

subplot(1,3,2);
imshow(xz);
title('XZ-plane, slice 128');

subplot(1,3,3);
imshow(yz);
title('YZ-plane, slice 128');
print("../results-3/A-slices", "-dpng");

%% Get B slices
[xy, xz, yz] = ortho_slices(B, 35, 64, 64);

%% Save head slice image
subplot(1,3,1);
imshow(xy);
title('XY-plane, slice 35');

subplot(1,3,2);
imshow(xz);
title('XZ-plane, slice 128');

subplot(1,3,3);
imshow(yz);
title('YZ-plane, slice 128');
print("../results-3/B-slices", "-dpng");
