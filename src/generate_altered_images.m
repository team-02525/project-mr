function [error_array, altered_images] = generate_altered_images(number_of_images, alter_function, test_image)
% GENERATE_ALTERED_IMAGES Generates a series of test images altered with
% the given function. One image has zero alteration, one has maximum
% alteration, others are equally spaced in between.
%
% Params:
%   'number_of_images': The number of images. Must be at least 2.
%   'alter_function': The function with which to alter the images. Must
%   take an image and a value between 0 and 1.
%   'test_image': The image to alter. DEFAULT: The function generates a
%   textured image with a triangle, a square and a circle.
%
% Returns:
%   'error_array': An array containing how much error the altered images
%   have.
%   'altered_images': The altered images.

if number_of_images < 2
    error("Number of images must be at least 2");
end


% Generate vector of effect sizes.
effect_size = (0:number_of_images)/number_of_images;

if nargin == 3
    if ~ismatrix(test_image) || diff(size(test_image))
        error("test_image must be a square matrix");
    else
        image_size = size(test_image, 1);
    end
else
    % Generate test image.
    image_size = 256;
    test_image = generate_simdata(image_size);
end

% Instantiate image and error arrays.
error_array = zeros(number_of_images, 1);
altered_images = zeros(image_size, image_size, number_of_images);

% For each effect size.
for i = 1:(number_of_images+1)
    % Generate altered image.
    altered_image = alter_function(test_image, effect_size(i));
    altered_images(:,:,i) = altered_image;
    
    % Calculate error.
    error_array(i) = error_measure(test_image, altered_image);
end