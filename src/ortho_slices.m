function [s01, s02, s03] = ortho_slices(mat, s1,s2,s3)
% ORTHO_SLICES computes indexed slices in respectively xy, xz and yz plane
% of a volume reconstruction
%
% Params:
% 'mat': Array of reconstructed slices
% 's1': Index of slice in the xy-plane
% 's2': Index of slice in the xy-plane
% 's3': Index of slice in the xy-plane
%
% Returns:
% 's01': Slice in xy-plane
% 's02': Slice in xz-plane
% 's02': Slice in yz-plane

% Assert array dimmensions
if ~(ndims(mat) == 3)
   error('Volume reconstruction must be array of slices'); 
end


% Assert slice indexes are positive and not larger than array size
if any([s1,s2,3]>size(mat)) || any([s1,s2,s3] < 0)
    error('Slice indices must positve and not larger that array size')
        
end

% XY-plane slice
s01 = squeeze(mat(:,:,s1));
% XZ-plane slice
s02 = squeeze(mat(:,s2,:));
% YZ-plane slice
s03 = squeeze(mat(s3,:,:));

end
