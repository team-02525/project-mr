%% Solution to assignment 2
% The following sections are solutions to various assigments and generate
% plots which are used in the report.


%% Read generated test images

% Previously generated with generate_simdata and generate_phantom
% respectively
test_image = read_image('../data/test-image.png'); 
phantom = read_image('../data/phantom.png');

% Create cell array of test images
test_images = {test_image, phantom};

%% Generate plot of each test image and 2D DFT

main_2_2(test_images);

%% Plot reconstruction of image with and without noise in frequency domain

main_2_4(test_images);

%% Plot reconstruction of image with and without sampling in frequency domain

main_2_5(test_images);

%% Plot grid of various percentages of noise and fraction sampled signal

grid_size = 3;
main_2_6_a(test_images{1}, grid_size)

%% Plot (noise, error)-graph and (frac, error)-graph 

num_points = 100;
main_2_6_b(test_images{1}, num_points)
